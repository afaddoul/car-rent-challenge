# Rent-a-Car Website Project

## Overview

This Nuxt 3 application is a high-fidelity prototype for a car rental service. Designed with a focus on front-end development, it features a responsive interface for browsing and selecting rental cars. The application was developed as part of a senior frontend developer assignment.

## Key Features

- **Atomic Design Structure**: The project is structured following atomic design principles, promoting high reusability and maintainability of components.
- **Responsive Design**: Ensures a seamless experience across both mobile and desktop devices.
- **Nuxt 3 with SSR**: Utilizes the latest version of Nuxt 3, optimized for Server-Side Rendering (SSR), enhancing performance and SEO.
- **Tailwind CSS with Class Variants Authority (CVA)**: The application is styled using Tailwind CSS and SCSS, simplified by leveraging CVA for an efficient handling of style variants.
- **Google Fonts Integration**: Implemented using the Google Fonts Nuxt module for appealing typography.
- **SVG Optimization**: Utilizes Nuxt SVGO for SVG file handling and optimization.
- **Image Optimization**: Incorporates Nuxt Image for optimized image loading and rendering.

## Technologies Used

- Nuxt 3 (SSR mode)
- Tailwind CSS 3
- Class Variants Authority (CVA)
- Pinia (Store structure present, functionality not implemented yet)
- Google Fonts Nuxt Module
- Nuxt SVGO
- Nuxt Image

## Installation and Setup

1. **Clone the Repository**
   ```bash
   git clone <repository-url>
   ```
2. **Navigate to the Project Directory**
   ```bash
   cd <project-directory>/web
   ```
3. **Install Dependencies**
   ```bash
    pnpm install
   ```
4. **Run the Project**
   ```bash
   pnpm run dev
   ```

## Project Routing

The application includes two main routes:

1. **Home Page (`/`):**

   - This is the landing page of the application where users can browse through the list of available rental cars.

2. **Car Details Page (`/car/[id]`):**
   - This route displays the detailed information about a specific car. It is accessible by clicking on a car in the home page list, and the `[id]` in the URL is replaced by the actual ID of the car.

These routes are designed to provide a straightforward navigation experience, allowing users to easily switch between viewing multiple cars and inspecting details of a particular car.
