export const useFavoriteCarsStore = defineStore("favoriteCars", () => {
  const favoriteCars = ref<string[]>([]);

  const isFavoriteCar = (carId: string) => {
    return favoriteCars.value.includes(carId);
  };

  const addCarToFavorites = (carId: string) => {
    favoriteCars.value.push(carId);
  };

  const removeCarFromFavorites = (carId: string) => {
    favoriteCars.value = favoriteCars.value.filter((id: string) => id !== carId);
  };

  return {
    favoriteCars,
    addCarToFavorites,
    removeCarFromFavorites,
    isFavoriteCar,
  };
});
