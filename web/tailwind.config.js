/** @type {import('tailwindcss').Config} */
const BREAKPOINTS = {
  xxs: "370px",
  xs: "480px",
  sm: "768px",
  md: "1024px",
  lg: "1280px",
  xl: "1440px",
  "2xl": "1920px",
};

export default {
  content: ["./**/*.{vue,ts,css}"],
  theme: {
    extend: {
      screens: BREAKPOINTS,
      fontFamily: {
        "plus-jakarta-sans": ["Plus Jakarta Sans"],
      },
      fontSize: {
        "3xl": ["2rem", "3rem"],
      },
      colors: {
        brand: {
          "primary-500": "#3563E9",
          "information-500": "#54A6FF",
          "secondary-300": "#90A3BF",
          "secondary-400": "#596780",
          "secondary-500": "#1A202C",
          "beau-blue": "#C3D4E966",
          "dark-gray": "#13131399",
          background: "#F6F7F9",
        },
      },
      borderRadius: {
        lg: "0.625rem",
      },
      spacing: {
        29: "7.25rem",
        30: "7.5rem",
        76: "19rem",
        97: "24.25rem",
      },
    },
  },
  plugins: [],
};
