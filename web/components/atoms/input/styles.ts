import { cva, type VariantProps } from "class-variance-authority";

export const input = cva(
  "rounded-[4px] text-sm font-plus-jakarta-sans font-medium px-4",
  {
    variants: {
      intent: {
        primary: "focus:outline-none focus-visible:outline-none",
      },
      size: {
        sm: "font-light",
      },
    },
  },
);

export type InputProps = VariantProps<typeof input>;
