import { cva, type VariantProps } from "class-variance-authority";

export const button = cva("text-white rounded px-5", {
  variants: {
    intent: {
      primary: "bg-brand-primary-500",
      secondary: "bg-brand-information-500",
    },
    size: {
      xxs: "h-9 w-[6.25rem] px-5 text-xs",
      xs: "h-11 w-32",
      sm: "h-11 w-29",
      md: "h-11 w-30",
      lg: "h-12 w-[9.75rem]",
    },
  },
});

export type ButtonProps = VariantProps<typeof button>;
