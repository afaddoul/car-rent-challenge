// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    "@nuxtjs/tailwindcss",
    "@nuxt/image",
    "@nuxtjs/google-fonts",
    "nuxt-svgo",
    "@pinia/nuxt",
  ],
  components: [
    {
      path: "~/components",
      pathPrefix: false,
      ignore: ["**/*.ts"],
    },
  ],
  image: {
    format: ["webp"],
  },
  googleFonts: {
    families: {
      "Plus Jakarta Sans": {
        wght: [100, 200, 300, 400, 500, 600, 700, 800],
      },
    },
  },
});
