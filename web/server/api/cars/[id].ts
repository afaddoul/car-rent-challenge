export default defineEventHandler((event) => {
  const id = getRouterParam(event, 'id');

  return {
    id,
    name:"Honda Accord",
    type: "Sedan",
    gasolineLiter: 65,
    description: "The Honda Accord is a series of automobiles manufactured by Honda since 1976, best known for its four-door sedan variant, which has been one of the best-selling cars in the United States since 1989.",
    kindOfTransition: "Manual",
    people: 5,
    pricePerDay: 60,
    img: "https://i.imgur.com/9nVToHU.png",
    images: [{
      url: "https://i.imgur.com/duo360F.jpeg",
    },
    {
      url:  "https://i.imgur.com/WAgGyaG.jpeg",
    }
  ],
  }
});
