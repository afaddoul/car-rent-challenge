export default defineEventHandler((event) => {

  return [
      {
        "name":"Honda Accord",
        "type":"Sedan",
        "gasolineLiter":65,
        "kindOfTransition":"Manual",
        "people":5,
        "pricePerDay":60,
        "id":"honda-accord",
        "img":"https://i.imgur.com/gQyBItF.png"
      },
      {
        "name":"Mazda 3",
        "type":"Hatchback",
        "gasolineLiter":55,
        "kindOfTransition":"Automatic",
        "people":5,
        "pricePerDay":45,
        "id":"mazda-3",
        "img":"https://i.imgur.com/KXX6ulR.png"
      },
      {
        "name":"Volkswagen Golf",
        "type":"Hatchback",
        "gasolineLiter":50,
        "kindOfTransition":"Manual",
        "people":5,
        "pricePerDay":55,
        "id":"volkswagen-golf",
        "img":"https://i.imgur.com/v9FuFLU.png"
      },
      {
        "name":"Audi A4",
        "type":"Sedan",
        "gasolineLiter":70,
        "kindOfTransition":"Automatic",
        "people":5,
        "pricePerDay":75,
        "id":"audi-a4",
        "img":"https://i.imgur.com/9nVToHU.png"
      }
  ]
})
