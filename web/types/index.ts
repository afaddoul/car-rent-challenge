export type Car = {
  id: string;
  name: string;
  type: string;
  gasolineLiter: number;
  kindOfTransition: string;
  people: number;
  pricePerDay: number;
  img: string;
};

export type CarDetails = {
  id: string;
  name: string;
  description: string;
  type: string;
  gasolineLiter: number;
  kindOfTransition: string;
  people: number;
  pricePerDay: number;
  img: string;
  images: {
    url: string;
  }[];
};

export type CarsResponse = {
  data: Car[];
  meta: {
    total: number;
    last_page: number;
  };
};
