module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    "eslint:recommended",
    "plugin:vue/vue3-recommended",
    "plugin:@typescript-eslint/recommended",
    "prettier",
  ],
  parser: ["vue-eslint-parser", "@typescript-eslint/parser"],
  overrides: [
    {
      env: {
        node: true,
      },
      files: [".eslintrc.{js,cjs}"],
      parserOptions: {
        sourceType: "script",
      },
    },
  ],
  parserOptions: {
    parser: "@typescript-eslint/parser",
  },
  plugins: ["vue", "prettier", "@typescript-eslint"],
  rules: {
    "vue/no-v-html": "off",
    "vue/multi-word-component-names": "off",
    "no-undef": "off",
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": ["error"],
    "vue/first-attribute-linebreak": "off",
  },
};
